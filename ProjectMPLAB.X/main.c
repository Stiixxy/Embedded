/*
 * File:   main.c
 *
 * Created on November 16, 2017, 2:21 PM
 */

//Setup configuration
#define _XTAL_FREQ 4000000

// CONFIG1
#pragma config FOSC = INTRC_NOCLKOUT// Oscillator Selection bits (INTOSCIO oscillator: I/O function on RA6/OSC2/CLKOUT pin, I/O function on RA7/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled and can be enabled by SWDTEN bit of the WDTCON register)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#pragma config MCLRE = OFF      // RE3/MCLR pin function select bit (RE3/MCLR pin function is digital input, MCLR internally tied to VDD)
#pragma config CP = OFF         // Code Protection bit (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Code Protection bit (Data memory code protection is disabled)
#pragma config BOREN = ON       // Brown Out Reset Selection bits (BOR enabled)
#pragma config IESO = OFF       // Internal External Switchover bit (Internal/External Switchover mode is disabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enabled bit (Fail-Safe Clock Monitor is disabled)
#pragma config LVP = ON         // Low Voltage Programming Enable bit (RB3/PGM pin has PGM function, low voltage programming enabled)

// CONFIG2
#pragma config BOR4V = BOR40V   // Brown-out Reset Selection bit (Brown-out Reset set to 4.0V)
#pragma config WRT = OFF        // Flash Program Memory Self Write Enable bits (Write protection off)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#define Final


#include <xc.h>
#include <pic16f887.h>
#include "RotaryEncoder.h"
#include "Relais.h"
#include "Adc.h" 
#include "IRReceiver.h"
#include "SPILib.h"

void Callback_Rotary(char);
void Callback_ADC(int);
void Loop();

#ifdef Test1
char current = 0;
void main(void) {
    
    OSCCON = 0b01100000; //Set clock for 4Mhz
    
    //Rotary_Setup(Callback_Rotary);
	Relais_Setup();

    while(1){
        Loop();
    }
    
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
    
	RELAISLAT = 15;
	current++;
	if(current == 4) current = 0;
	Turn_On_Relais(current);
	int delay;
    for(delay = 30000; delay > 0; delay--);
    Turn_Off_Relais(current);
    
}

//Automatically called when rotary has a change (by rotary check))
void Callback_Rotary(char c){
    
}
#endif

#ifdef Test2
void main(void) {
  
    OSCCON = 0b01100000; //Set clock for 4Mhz
    
    Rotary_Setup(Callback_Rotary);
	Relais_Setup();
	
    while(1){
        Loop();
    }
	
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
	Toggle_Relais(0);
    int delay;
    for(delay = 30000; delay > 0; delay--);
}

//Automatically called when rotary has a change (by rotary check))
void Callback_Rotary(char c){
    
}
#endif

#ifdef Test3
char current = 0;
void main(void) {

    OSCCON = 0b01100000; //Set clock for 4Mhz
    Rotary_Setup(Callback_Rotary);
	Relais_Setup();
	
    while(1){
        Loop();
	}
    
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
	Rotary_Check();
}

//Automatically called when rotary has a change (by rotary check))
void Callback_Rotary(char c){
	Turn_Off_Relais(current);
    current++;
	if(current == 4) current = 0;
	Turn_On_Relais(current);
}
#endif

#ifdef Test4
char selectedOutput = 0;

void main(void) {
    
    OSCCON = 0b01100000; //Set clock for 4Mhz
  
    Rotary_Setup(Callback_Rotary);
	Relais_Setup();
	
    while(1){
        Loop();
	}
        
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
	Rotary_Check();
}

//Automatically called when rotary has a change (by rotary check))
void Callback_Rotary(char c){
    
	Turn_Off_Relais(selectedOutput);
	
	if(!c){
		//Clockwise rotation
		selectedOutput++;
		
		if(selectedOutput == RELAISCOUNT) selectedOutput = 0;
	}else{
		//Counter clockwise rotation
		selectedOutput--;
		
		if(selectedOutput > RELAISCOUNT) selectedOutput = RELAISCOUNT - 1;
	}
	
	Turn_On_Relais(selectedOutput);
}
#endif

#ifdef Test5
void main(void) {

    OSCCON = 0b01100000; //Set clock for 4Mhz
    Rotary_Setup(Callback_Rotary);
	Relais_Setup();
    Init_ADC();
    
    while(1){
        Loop();
	}
    
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
    char value = Get_ADC_Value();

	char stepSize = 0b11111111;		//Calculate the stepsize by the max / the steps
	stepSize /= 4;
	char onLeds = value / stepSize;	//Calculate the amouth of leds that are on
    onLeds++;
    
    RELAISLAT = 15;
    
    char i;
    for(i = 0; i < onLeds; i++){
        Turn_On_Relais(i);
    }
}

//Automatically called when rotary has a change (by rotary check))
void Callback_Rotary(char c){
    
}
#endif

#ifdef Test6
void main(void) {

    OSCCON = 0b01100000; //Set clock for 4Mhz
    Rotary_Setup(Callback_Rotary);
	Relais_Setup();
    Init_ADC();
    
    while(1){
        Loop();
	}
    
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
    Check_ADC();
    
    Turn_Off_All_Relais();
    
    char i;
    for(i = 0; i < onLeds; i++){
        Turn_On_Relais(i);
    }
}

//Automatically called when rotary has a change (by rotary check))
void Callback_Rotary(char c){
    
}
#endif

#ifdef Test7
char current = 0;
void main(void) {

    OSCCON = 0b01100000; //Set clock for 4Mhz
	Relais_Setup();
    Init_IR_Receiver();
    
    while(1){
        Loop();
	}
    
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
    if(receivedByte){//receivedbyte is not null
		//Turn_Off_Relais(current);
		current++;
		if(current == 4) current = 0;
		Turn_On_Relais(current);
		receivedByte = 0;
	}
}


void interrupt ISR(){
	if(INTCONbits.RBIF)
		IR_IOC();
	if(INTCONbits.T0IF)
		IR_Timer_Int();
}
#endif

#ifdef Test8
char current = 0;
void main(void) {

    OSCCON = 0b01100000; //Set clock for 4Mhz
	Relais_Setup();
    Init_IR_Receiver();
    
    while(1){
        Loop();
	}
    
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
    if(receivedByte){//receivedbyte is not null
		switch(receivedByte){
		case Input0Key:
			current = 0;
			break;
		case Input1Key:
			current = 1;
			break;
		case Input2Key:
			current = 2;
			break;
		case Input3Key:
			current = 3;
			break;
		case VolUp:
			current++;
			if(current >= RELAISCOUNT) current = 0;
			break;
		case VolDown:
			if(current == 0) current = RELAISCOUNT; //Relaiscount is 1 to high but the current -- fixed that anyway
			current--;
			break;
		}
		
        
		Turn_On_Relais(current);
        //Reset_IR();
		receivedByte = 0;
    }
}


void interrupt ISR(){
	if(INTCONbits.RBIF)
		IR_IOC();
	if(INTCONbits.T0IF)
		IR_Timer_Int();
}
#endif

#ifdef Test9
char current = 0;
void main(void) {

    OSCCON = 0b01100000; //Set clock for 4Mhz
	Relais_Setup();
    Init_IR_Receiver();
    
    while(1){
        Loop();
	}
    
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
    if(receivedByte){//receivedbyte is not null
		switch(receivedByte){
		case Input0Key:
			current = 0;
			break;
		case Input1Key:
			current = 1;
			break;
		case Input2Key:
			current = 2;
			break;
		case Input3Key:
			current = 3;
			break;
		case VolUp:
			PORTCbits.RC0	= 1;
			PORTCbits.RC1	= 0;
			//T1CONbits.TMR1ON = 1;
			//TMR1 = Timer1Time;
			break;
		case VolDown:
   			PORTCbits.RC0	= 0;
			PORTCbits.RC1	= 1;
			//T1CONbits.TMR1ON = 1;
			//TMR1 = Timer1Time;
			break;
		}
		
        //current++;
		Turn_On_Relais(current);
        receivedByte = 0;
	}
}

void interrupt ISR(){
	if(INTCONbits.RBIF)
		IR_IOC();
	if(INTCONbits.T0IF)
		IR_Timer_Int();
	if(PIR1bits.TMR1IF){
		PIR1bits.TMR1IF = 0;
		TMR1 = Timer1Time;
		T1CONbits.TMR1ON = 0;
		
		//Disable motor
		PORTCbits.RC0			= 0;
		PORTCbits.RC1			= 0;
	}
}
#endif

#ifdef Test10

char current = 0;
void main(void) {
    OSCCON = 0b01100000; //Set clock for 4Mhz
	Relais_Setup();
    SPI_Init();
        
	//SPI_Write_Dot(char_g, 5, DISPLAYR);
    SPI_Write_Dot_String("TEST 123", DISPLAYR);
    __delay_ms(200);
    SPI_Write_Dot_Volume("VOL:XXXX", 20, DISPLAYR);
    __delay_ms(200);*
    
    int i;
    while(i < 2000){
        SPI_Write_Dot_Volume("VOL:XXXX", i, DISPLAYR);
        i += 257;
        __delay_ms(200);
    }
    
    while(1){
        Loop();
	}
	
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
    
}

void interrupt ISR(){
	
    return;
}
#endif

#ifdef Test11

char current = 0;
void main(void) {

    OSCCON = 0b01100000; //Set clock for 4Mhz
	Relais_Setup();
    SPI_Init();
	Init_ADC(Callback_ADC);
    
	SPI_Write_Dot_String("Running ", DISPLAYR);
	
    while(1){
        Loop();
	}
	
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
    Check_ADC();
}

void Callback_ADC(char value){
	SPI_Write_Dot_Volume("VOL:    ", value, DISPLAYR);
}

void interrupt ISR(){
	
    return;
}
#endif

#ifdef Final
void Handle_IR();

char currentChannel = 0;
char currentVolume = 0;
void main(void) {

    OSCCON = 0b01100000; //Set clock for 4Mhz
	
	//Init modules
	Relais_Setup();
    Rotary_Setup(Callback_Rotary);    
	Init_ADC(Callback_ADC);
    Init_IR_Receiver();
	SPI_Init();
	
	currentChannel = EEPROM_READ(0x00);
	
	SPI_Write_Dot_String("HELLO   ", DISPLAYL);
	SPI_Write_Dot_String("USER    ", DISPLAYR);
	
    while(1){
        Loop();
	}
	
    return;
}

//Called as fast as possible on repeat by main
void Loop(){
    Check_ADC();
	Rotary_Check();
	if(receivedByte)//receivedbyte is not null
		Handle_IR();
}

void Callback_Rotary(char dir){
	//Turn_Off_Relais(currentChannel);
	if(!dir){
		currentChannel += 1;
		currentChannel %= 4;
	}else{
		if(currentChannel == 0) currentChannel = 4;
		currentChannel -= 1;
	}
	Turn_On_Relais(currentChannel);
	EEPROM_WRITE(0x00, currentChannel);
	SPI_Write_Dot_Volume("CHAN:   ", currentChannel, DISPLAYR);
}

void Callback_ADC(int volume){
	currentVolume = volume;
	SPI_Write_Dot_Volume("VOL:    ", volume, DISPLAYL);
}

void Calibrate_ADC(){
	receivedByte = 0;
	char i;
	//We calibrate until a new key is received from the remote
	MaxADC = 0;
	MinADC = 1023;
	SPI_Write_Dot_String("SETUP   ", DISPLAYL);
    SPI_Write_Dot_String("STARTED ", DISPLAYR);
    __delay_ms(1000);
	while(!receivedByte){
		int adcVal = Get_ADC_Value();
		if(adcVal > MaxADC) MaxADC = adcVal;
		if(adcVal < MinADC) MinADC = adcVal;
		
		//Show value
		char message[9]  = "        "; 
		
		//max on right
		for(i = 0; i < 4; i++){
			if(MaxADC == 0 && i != 0){
				break;
			}
			message[7-i] = MaxADC % 10 + '0';
			MaxADC /= 10;
		}
		
		//min on left
		for(i = 0; i < 4; i++){
			if(MinADC == 0 && i != 0){
				break;
			}
			message[4-i] = MinADC % 10 + '0';
			MinADC /= 10;
		}
		
		SPI_Write_Dot_String(message, DISPLAYR);
	}		
	//Write to eeprom
	EEPROM_WRITE(0x02, MinADC & 255);
	EEPROM_WRITE(0x03, MinADC >> 8);
	
	EEPROM_WRITE(0x04, MaxADC & 255);
	EEPROM_WRITE(0x05, MaxADC >> 8);
	StepSize = (MaxADC / MaxVolume + 1) - Gap; //1024 is
    
    //Write a special caracter to eeprom so next time we start up
    //We know that we have calibrated this setup
    EEPROM_WRITE(0x06, CalibrationKey);
}

void Handle_IR(){
	switch(receivedByte){
		case Input0Key:
			currentChannel = 0;
			break;
		case Input1Key:
			currentChannel = 1;
			break;
		case Input2Key:
			currentChannel = 2;
			break;
		case Input3Key:
			currentChannel = 3;
			break;
		case VolUp:
			PORTCbits.RC0	= 1;
			PORTCbits.RC1	= 0;
			T1CONbits.TMR1ON = 1;
			TMR1 = Timer1Time;
			break;
		case VolDown:
   			PORTCbits.RC0	= 0;
			PORTCbits.RC1	= 1;
			T1CONbits.TMR1ON = 1;
			TMR1 = Timer1Time;
			break;
		case Calibrate:
			//Calibrate_ADC();
			break;
	}
	
	Turn_On_Relais(currentChannel);
	SPI_Write_Dot_Volume("CHAN:   ", currentChannel, DISPLAYL);
    receivedByte = 0;
}

void interrupt ISR(){
	if(INTCONbits.RBIF)
		IR_IOC();
	if(INTCONbits.T0IF)
		IR_Timer_Int();
	if(PIR1bits.TMR1IF){
		PIR1bits.TMR1IF = 0;
		TMR1 = Timer1Time;
		T1CONbits.TMR1ON = 0;
		
		//Disable motor
		PORTCbits.RC0			= 0;
		PORTCbits.RC1			= 0;
	}
    return;
}
#endif
