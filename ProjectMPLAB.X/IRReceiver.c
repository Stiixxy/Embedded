#include "IRReceiver.h"

void Init_IR_Receiver(){
	
	//Setup the pin
	TRISBbits.TRISB0	= 1; //RB0 is input
	ANSELHbits.ANS12	= 0; //RB0 is digital
	
	//Setup IOC on rising edge
	OPTION_REGbits.INTEDG	= 1; //Set interrupt on rising edge
	IOCBbits.IOCB0			= 1; //Enable interrupts for pin RB0
	INTCONbits.RBIE			= 1; //Enable PORTB change interrupt
	
	//Setup Timer0 based on 4Mhz
	OPTION_REGbits.PSA		= 0; //Set prescaler to timer0
	OPTION_REGbits.PS		= 0b010; //Prescaler 8, geeft 4Mhz / 2^8 = 125kHz
	INTCONbits.T0IE			= 1; //Enable timer0 interrupt
	OPTION_REGbits.T0CS		= 0; //Setup timer 0 on clock
	INTCONbits.T0IF			= 0; //Make sure the interupt flag is cleared
	TMR0					= Timer0Time;
	Pause_Timer();
	
	//Timer1 Registers Prescaler= 2 - TMR1 Preset = 20536 - Freq = 11.11 Hz - Period = 0.090000 seconds
    T1CONbits.T1CKPS1 = 0;   // bits 5-4  Prescaler Rate Select bits
    T1CONbits.T1CKPS0 = 1;   // bit 4
    T1CONbits.T1OSCEN = 1;   // bit 3 Timer1 Oscillator Enable Control bit 1 = on   
    T1CONbits.T1SYNC = 1;    // bit 2 Timer1 External Clock Input Synchronization Control bit...1 = Do not synchronize external clock input
    T1CONbits.TMR1CS = 0;    // bit 1 Timer1 Clock Source Select bit...0 = Internal clock (FOSC/4)
    T1CONbits.TMR1ON = 0;    // bit 0 enables timer
    TMR1 = Timer1Time;
    PIE1bits.TMR1IE = 1;
    PIR1bits.TMR1IF = 0;
	
	//Setup motor
	TRISCbits.TRISC0		= 0;
	TRISCbits.TRISC1		= 0;
	PORTCbits.RC0			= 0;
	PORTCbits.RC1			= 0;
	
	INTCONbits.PEIE			= 1; //Emable pheripheral interupts
	INTCONbits.GIE			= 1; //Enable global interrupts
}

void IR_IOC(){
	//An interrupt occured. Set timer to 0.4 seconds and read bit after the timer interrupt
	INTCONbits.RBIF			= 0;
    if(PORTBbits.RB0 == 0) return; //Als tijd over bij testmoment, probeer zonder deze regel
	TMR0					= Timer0Time;
	Resume_Timer();
}

void Reset_IR(){
    currentBit = 0; //Bij de hoeveelste bit zijn we 
	receivingByte = 0;
	lastByte = 0;
}

void IR_Timer_Int(){
	
	INTCONbits.T0IF			= 0;
	Pause_Timer();
	 
	if(currentBit > 3){
		//Skip C0-3 and H bit
		//VBIT(receivingByte, currentBit - 4, !PORTBbits.RB0);
		receivingByte <<= 1;
		//Check of we RB0 wel moete complimenten 
		//VBIT(receivingByte, 0, !PORTBbits.RB0);
        receivingByte |= !(PORTB & 1);
	}
	
	currentBit++;
	if(currentBit == 12){
		if(lastByte){
			//This is the second received byte
			receivedByte = receivingByte;
			lastByte = 0;
		}else{
			//This is the first received byte
			lastByte = receivingByte;
		}
		currentBit = 0;
		receivingByte = 0;
	}
	
}

void Pause_Timer(){
	OPTION_REGbits.T0CS		= 1;
}

void Resume_Timer(){
	OPTION_REGbits.T0CS		= 0;
}
