#ifndef SPI_H
#define	SPI_H

#include <xc.h>
#include <pic16f887.h>

#define bool char
#define TRUE 1
#define FALSE 0

#define DISPLAYR 1
#define DISPLAYL 2

#define DOTREGISTER 1
#define CONTROLREGISTER 2

#ifndef _XTAL_FREQ
#define _XTAL_FREQ 4000000 //right?
#endif

void SPI_Init();

void SPI_Write_Char(char);
void SPI_Write_String(char, char *);

void SPI_Reset_Display(char);
void SPI_Enable_Display(char);
void SPI_Blank_Display(char, bool);
void SPI_Select_Register(char, char); //? check this out
void SPI_Register_To_Latch(char);
void SPI_Register_To_Word(char);

#endif	/* SPI_H */