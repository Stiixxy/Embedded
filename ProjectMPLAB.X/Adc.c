#include "Adc.h"


void Init_ADC(void (*call)(int)){
    //ADCON0
    ADCON0bits.ADCS = 00;
    ADCON0bits.CHS  = ADCSELECT;
    ADCON0bits.ADON = 1;
    
    //ADCON1
    ADCON1bits.ADFM = 1; //Right justifed, we want the entire 10 bits
    
    //Pot
    Init_Pot(); 
	
	MinADC = 0;
    MaxADC = 1023;
    
    //Calibration (not used)
    if(EEPROM_READ(0x6) == CalibrationKey){
        MinADC += EEPROM_READ(0x02);
        MinADC += (EEPROM_READ(0x03) << 8);
        
        MaxADC = 0;
        MaxADC += EEPROM_READ(0x04);
        MaxADC += (EEPROM_READ(0x05) << 8);
    }
	
    //calculate stepsize
	StepSize = ((MaxADC - MinADC) / (MaxVolume)) - Gap; //1024 is
	
    callbackadc = call;
}

void Init_Pot(){
    POTTRIS = 1;
    POTANS  = 1;
}

int Get_ADC_Value(){
    __delay_ms(1);
    ADCON0bits.GO = 1;
    
    while(ADCON0bits.GO)
        continue;
    
    return (ADRESH << 8) + ADRESL;
}

int Get_Volume(int input){
    int volume = input / StepSize;
    
    return volume;
}

void Check_ADC(){
	static int currentVolume = -1; //Make this a weird value so it is never the same as what volume starts out with
    int value = Get_ADC_Value();
	int volume = MinVolume;
	int current = MinADC; //Min
	
	while(!(value > current && value < current + StepSize)){
		current += StepSize; // + Gap;
		if(value < current + Gap) return;  //We are in a gap, dont update
		current += Gap;
		
		volume++;
        if(volume == MaxVolume) break;
	}
	
    if(volume == currentVolume) return; //Nothing changed
	currentVolume = volume;
    callbackadc(volume);
}

int Check_Diff(int first,int second,int threshhold){
    char diff;
    if(first>second){
        diff = first - second;
    }else{
        diff = second - first;
    }
	if(diff > threshhold) return 1;
    return 0;
}
