/********************************************************************
 Universal startup code for new and simple projects

 PIC   : 16F1829
 Board : PICkit 3 Low Pin Count Demo Board
 Date  : 27-08-2013
********************************************************************/

//#include <xc.h>               // PIC hardware mapping
#include <pic16f1829.h>
#include "Adc.h"
#define _XTAL_FREQ 500000     // Used by the XC8 delay_ms(x) macro

//config bits that are part-specific for the PIC16F1829
#pragma config FOSC=INTOSC, WDTE=OFF, PWRTE=OFF, MCLRE=OFF, CP=OFF, \
               CPD=OFF, BOREN=ON, CLKOUTEN=OFF, IESO=OFF, FCMEN=OFF
#pragma config WRT=OFF, PLLEN=OFF, STVREN=OFF, BORV=LO, LVP=OFF

void Loop();
void HandleADC(int);

void main(void){
    OSCCON = 0b00111000;      // 500KHz clock speed, change if needed
	
	// Do initialisation here
	InitADC(HandleADC);
    TRISC = 0;
    LATC = 0;
    
    LATC = 2;
    
    while(1){
		Loop();
    }
}

void Loop(){
    
    CheckADC();
    
}

void HandleADC(int volume){
    
    LATC = volume;
    
}