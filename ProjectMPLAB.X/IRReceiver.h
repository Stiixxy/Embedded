#ifndef IRRECEIVER_H
#define	IRRECEIVER_H

#include <xc.h>
#include <pic16f887.h>

char receivedByte = 0; //0 when nothing was recieved yet

void Init_IR_Receiver();
void IR_IOC();			//Called when pic received IOC
void IR_Timer_Int();

void Pause_Timer();
void Resume_Timer();

void Reset_IR();

// Bit manipulation by Sander en Martijn (Pictris source code regel 45)
#define SBIT(reg,bit)	reg |=  (1<<bit)							// Macro defined for Setting  a bit of any register.
#define CBIT(reg,bit)	reg &= ~(1<<bit)							// Macro defined for Clearing a bit of any register.
#define GBIT(reg,bit)   reg &   (1<<bit)							// Macro defined for Getting  a bit of any register.
#define TBIT(reg,bit)   reg ^=  (1<<bit)							// Macro defined for Toggling a bit of any register.
#define VBIT(reg,bit,v) reg  = (reg & ~(1<<bit)) | ((v&1)<<bit)		// Macro defined for setting  a bit of any register to `v'

#define Input0Key	0b10010000 //0b01101111//
#define Input1Key	0b00001000 //0b11110111
#define Input2Key	0b10100000
#define Input3Key	0b10001000

#define VolUp		0b00100000
#define VolDown		0b00010000
#define Calibrate	0b00111000

//http://prntscr.com/hj2zft
#define Timer0Time	190 ///156 //.8 ms delay
#define Timer1Time	20536 //Gives 0.9 ms delay http://prntscr.com/hj34wb
#define T2ON		T2CONbits.TMR2ON


 char currentBit = 0; //Bij de hoeveelste bit zijn we 
	 char receivingByte = 0;
	 char lastByte = 0;
#endif	/* IRRECEIVER_H */
