#ifndef ADC_H
#define	ADC_H

#include <xc.h>

#define TRUE 1
#define FALSE 0

#define _XTAL_FREQ 500000     // Used by the XC8 delay_ms(x) macro

#define CalibrationKey 129

#define POTTRIS     TRISEbits.TRISE2
#define POTANS      ANSELbits.ANS7
#define ADCSELECT   0b00011

#define MaxVolume    15
#define MinVolume    0
#define Gap			 2
int MaxADC = 1023;
int MinADC = 0;
int StepSize; 

static void (*callbackadc)(int) = 0; //Passes 1 as parameter when Clockwise 0 when CC

void InitADC(void (*call)(int));
void InitPot();
void CheckADC();
int GetADCValue();
int GetVolume(int input);

int CheckDiff(int first,int second,int threshhold);

#endif	/* ADC_H */