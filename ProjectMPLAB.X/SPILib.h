#ifndef SPILIB_H
#define	SPILIB_H

#include <xc.h>
#include "Characters.h"

#define RESETJ1 PORTCbits.RC2
#define CEJ1    PORTCbits.RC4
#define BLANKJ1 PORTCbits.RC6
#define RSJ1    PORTCbits.RC7

#define RESETJ2 PORTDbits.RD2
#define CEJ2    PORTDbits.RD4
#define BLANKJ2 PORTDbits.RD6
#define RSJ2    PORTDbits.RD7

#define DISPLAYL 1
#define DISPLAYR 2

void SPI_Init();

void SPI_Write_Char(char);

void SPI_Write_Control_Word(char, char);
void SPI_Write_Dot(char*, char); //Always 320 bits, so 40 bytes
void SPI_Write_Dot_Volume(char* data, char display, char volume);

void SPI_Write_Dot_String(char*, char);
void SPI_Write_Letter(char*);

void SPI_Reset_Display(char);
void SPI_Set_Blank(char);
void SPI_Clear_Blank(char);
void SPI_Clear_Display(char);

char* Get_Letter(char);

#endif	/* SPILIB_H */
