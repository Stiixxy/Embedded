#include "SPILib.h"


void SPI_Init(){
    
    //Interupts
    PIE1bits.SSPIE  = 0;
    INTCONbits.PEIE = 1;
    INTCONbits.GIE  = 1;
    
    //SSPCON
    SSPCONbits.CKP  = 0;
    SSPCONbits.SSPM = 0b0000; /////////
    
    //SSPSTAT
    SSPSTATbits.CKE = 1;
    
    //Set clock and SDO
	TRISCbits.TRISC3 = 0;
	TRISCbits.TRISC5 = 0;
	
	//Set J1 outputs
	TRISCbits.TRISC2 = 0;
	TRISCbits.TRISC4 = 0;
	TRISCbits.TRISC6 = 0;
	TRISCbits.TRISC7 = 0;
	
	//Set J2 outputs
	TRISDbits.TRISD2 = 0;
	TRISDbits.TRISD4 = 0;
	TRISDbits.TRISD6 = 0;
	TRISDbits.TRISD7 = 0;
    
	//Make pins 0
	PORTD = 0;
	PORTC = 0;
	
    //Default HIGH
    CEJ1 = 1;
    CEJ2 = 1;
    
    RESETJ1 = 1;
    RESETJ2 = 1;

    //Finally enable SSP
    SSPCONbits.SSPEN = 1;
	
	//make sure int flag is low
    PIR1bits.SSPIF = 0;
    
	//Init displays
	SPI_Reset_Display(DISPLAYR);
	SPI_Reset_Display(DISPLAYL);
	
	SPI_Clear_Display(DISPLAYR);
	SPI_Clear_Display(DISPLAYL);

	SPI_Write_Control_Word(0b10000001, DISPLAYR);
    SPI_Write_Control_Word(0b01111111, DISPLAYR);
	
	SPI_Write_Control_Word(0b10000001, DISPLAYL);
    SPI_Write_Control_Word(0b01111111, DISPLAYL);

}
#ifndef _XTAL_FREQ
#define _XTAL_FREQ 4000000
#endif
void SPI_Write_Char(char byte){
    
    //char tmp = SSPBUF;
    //SSPBUF = byte;
    SSPBUF = byte;
    while(!PIR1bits.SSPIF){
       
        continue;
    }
    PIR1bits.SSPIF = 0;
    
}


void SPI_Write_Control_Word(char byte, char display){
    
    //Select control register
    if(display == DISPLAYR){
        RSJ1 = 1;
        //Delay?
        CEJ1 = 0;
    }else{
        RSJ2 = 1;
        //Delay
        CEJ2 = 0;
    }
    
    //Write byte
    SPI_Write_Char(byte);
    
    //Copy control word
    if(display == DISPLAYR){
        CEJ1 = 1;
    }else{
        CEJ2 = 1;
    }
    
}

void SPI_Write_Dot(char* bytes , char display){
    
     //Select dot register
    if(display == DISPLAYR){
        RSJ1 = 0;
        //Delay?
        CEJ1 = 0;
    }else{
        RSJ2 = 0;
        //Delay
        CEJ2 = 0;
    }
    
    //Write bye
    char i;
    for(i = 0; i < 40; i++){
		SPI_Write_Char(bytes[i]);
    }
    
    //Copy dot
    if(display == DISPLAYR){
        CEJ1 = 1;
    }else{
        CEJ2 = 1;
    }
    
}


void SPI_Write_Dot_String(char* bytes, char display){
    //Select dot register
    if(display == DISPLAYR){
        RSJ1 = 0;
        //Delay?
        CEJ1 = 0;
    }else{
        RSJ2 = 0;
        //Delay
        CEJ2 = 0;
    }
    
    //Write to display
    char i;
    for(i = 0; i < 8; i++){
        SPI_Write_Letter(Get_Letter(bytes[i]));    
    }
    
    //Copy dot
    if(display == DISPLAYR){
        CEJ1 = 1;
    }else{
        CEJ2 = 1;
    }
}

void SPI_Write_Dot_Volume(char* data, char volume , char display){
    char i;
    char newdata[8];
	for(i = 0; i < 8; i++){
		newdata[i] = data[i];
	}
    for(i = 0; i < 4; i++){
        if(volume == 0 && i != 0){
            break;
        }
        newdata[7-i] = volume % 10 + '0';
        volume /= 10;
    }
    
    SPI_Write_Dot_String(newdata , display);
}

void SPI_Write_Letter(char* data){
    char i;
    for(i = 0; i < 5; i++){
        SPI_Write_Char(data[i]);
    }
}

void SPI_Reset_Display(char display){
    
    if(display == DISPLAYR){
        RESETJ1 = 0;
        __delay_ms(10);
        RESETJ1 = 1;
    }else{
        RESETJ2 = 0;
        __delay_ms(10);
        //delay
        RESETJ2 = 1;
    }
    
}

void SPI_Set_Blank(char display){
    
    if(display == DISPLAYR){
        BLANKJ1 = 1;
    }else{
        BLANKJ2 = 1;
    }
    
}

void SPI_Clear_Blank(char display){
    
    if(display == DISPLAYR){
        BLANKJ1 = 0;
    }else{
        BLANKJ2 = 0;
    }
    
}

void SPI_Clear_Display(char display){
	char bytes[] = {0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0};
	SPI_Write_Dot(bytes, display);
	
}

char* Get_Letter(char letter){
    if(letter >= 'A' && letter <= 'Z') //It is in the letter range of the ascii table
        return _CHARACTERS[letter - 'A']; //A in our table starts at 0, map the ascii A to our A
    else if(letter == ' ')
        return _CHARACTERS[26];
    if(letter >= '0' && letter <= '9')
        return _CHARACTERS[letter - '0' + 27];
    if(letter == ':')
        return _CHARACTERS[37];

    return _CHARACTERS[27];          //No match, return '0'
}
