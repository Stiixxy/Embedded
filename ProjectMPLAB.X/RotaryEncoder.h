#ifndef ROTARY_H
#define ROTARY_H

#include <pic16f887.h>
#include <xc.h>

#define TRUE 1
#define FALSE 0

#define PINATRIS  TRISBbits.TRISB4
#define PINBTRIS  TRISBbits.TRISB5
#define PINAANSEL ANSELHbits.ANS11
#define PINBANSEL ANSELHbits.ANS13
#define PINAInput PORTBbits.RB4   //Location of pin a
#define PINBInput PORTBbits.RB5   //Location of pin b

static void (*callback)(char) = 0; //Passes 1 as parameter when Clockwise 0 when CC
static char lastValue;

void Rotary_Setup(void (*call)(char));
void Rotary_Check();   //Calls callback when change occured

#endif