#ifndef ADC_H
#define	ADC_H

#include <pic16f887.h>
#include <xc.h>

#define TRUE 1
#define FALSE 0

#define _XTAL_FREQ 500000

#define CalibrationKey 129

#define POTTRIS     TRISEbits.TRISE2
#define POTANS      ANSELbits.ANS7
#define ADCSELECT   0b0111

#define MaxVolume    50
#define MinVolume    0
#define Gap			 2
int MaxADC = 1023;
int MinADC = 0;
int StepSize; //1024 is

static void (*callbackadc)(int) = 0; //Passes 1 as parameter when Clockwise 0 when CC

void Init_ADC(void (*call)(int));
void Init_Pot();
void Check_ADC();
int Get_ADC_Value();
int Get_Volume(int input);

int Check_Diff(int first,int second,int threshhold);

#endif	/* ADC_H */
