#ifndef RELAIS_H
#define	RELAIS_H

#include <pic16f887.h>
#include <xc.h>

#define TRUE 1
#define FALSE 0

#define RELAISTRIS  TRISA
#define RELAISLAT	PORTA //No LATA?

#define RELAISCOUNT 4

void Relais_Setup();
void Turn_On_Relais(char number);
void Turn_Off_Relais(char number);
char Relais_Is_Enabled(char number);
void Toggle_Relais(char number);
void Select_Relais(char number);
void Turn_Off_All_Relais();

// Bit manipulation by Sander en Martijn (Pictris source code regel 45)
#define SBIT(reg,bit)	reg |=  (1<<bit)							// Macro defined for Setting  a bit of any register.
#define CBIT(reg,bit)	reg &= ~(1<<bit)								// Macro defined for Clearing a bit of any register.
#define GBIT(reg,bit)   reg &   (1<<bit)							// Macro defined for Getting  a bit of any register.
#define TBIT(reg,bit)   reg ^=  (1<<bit)							// Macro defined for Toggling a bit of any register.
#define VBIT(reg,bit,v) reg  = (reg & ~(1<<bit)) | ((v&1)<<bit)		// Macro defined for setting  a bit of any register to `v'

#endif	/* RELAIS_H */
