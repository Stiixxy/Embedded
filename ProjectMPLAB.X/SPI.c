#include "SPI.h"


void SPI_Init(){
	//TABLE 13-2 for all SPI registers
	
	//Enable SPI interrupt
	PIE1bits.SSPIE = 1;
	INTCONbits.PEIE = 1;
	INTCONbits.GIE = 1;
	
	//SSPCON
	SSPCONbits.CKP = 0; //Idle state is low? check dot datasheet
	SSPCONbits.SSPM = 0b0000; //SPI master mode clock = fosc/4
	
	//SSPSTAT (bit 7-6 only matter for SPI)
	SSPSTATbits.CKE = 1; //CPK = 0, data trasnsmitted on rising edge of CKC
	
	//I/O pins config datasheet
	//Required pins
	//SCK: Output RC3
	//SDO: output RC5
	//SDI: input but not required for this project RC4
	//not SS: Input but we are not a slave
	
	//IO handleiding		J1		J2
	//Reset					RC2		RD2 
	//CE					RC4		RD4 
	//Blank					RC6		RD6 
	//RS(register select)	RC7		RD7  
	
	//Clock						RC3
	//SDO						RC5
	
	//Set clock and SDO
	TRISCbits.TRISC3 = 0;
	TRISCbits.TRISC5 = 0;
	
	//Set J1 outputs
	TRISCbits.TRISC2 = 0;
	TRISCbits.TRISC4 = 0;
	TRISCbits.TRISC6 = 0;
	TRISCbits.TRISC7 = 0;
	
	//Set J2 outputs
	TRISDbits.TRISD2 = 0;
	TRISDbits.TRISD4 = 0;
	TRISDbits.TRISD6 = 0;
	TRISDbits.TRISD7 = 0;
	
	//Logic HIGHS
	//Reset
	PORTCbits.RC2	 = 1;
	PORTDbits.RD2	 = 1;
    
    PORTCbits.RC4    = 1; 
    PORTDbits.RD4    = 1;

	SSPCONbits.SSPEN = 1; //Enable serial pins (pins in and output must still be configured)
	
}

void SPI_Reset_Display(char display){
	
	if(display == DISPLAYR){
		PORTCbits.RC2 = 0;
	}else{
		PORTDbits.RD2 = 0;
	}
	
	__delay_us(50); //uit dot datasheet klopt maybe niet
	
	if(display == DISPLAYR){
		PORTCbits.RC2 = 1;
	}else{
		PORTDbits.RD2 = 1;
	}
	
}

void SPI_Enable_Display(char display){
	
	if(display == DISPLAYR){
		PORTCbits.RC4 = 1;
		PORTDbits.RD4 = 0;
	}else{
		PORTCbits.RC4 = 0;
		PORTDbits.RD4 = 1;
	}
	
}

void SPI_Blank_Display(char display, bool b){
	
	if(display == DISPLAYR){
		TRISCbits.TRISC6 = b;
	}else{
		TRISDbits.TRISD6 = b;
	}
	
}

void SPI_Select_Register(char display, char r){
	//Dont know at all if this works
	if(display == DISPLAYR){
		PORTCbits.RC4 = 1;
		if(r == DOTREGISTER){
			PORTCbits.RC7 = 0;
		}else{
			PORTCbits.RC7 = 1;
		}
		__delay_us(50); //Random number here
		PORTCbits.RC4 = 0;
		__delay_us(50); //Random number here
		PORTCbits.RC4 = 1;
	}else{
		PORTDbits.RD4 = 1;
		if(r == DOTREGISTER){
			PORTDbits.RD7 = 0;
		}else{
			PORTDbits.RD7 = 1;
		}
		__delay_us(50); //Random number here
		PORTDbits.RD4 = 0;
		__delay_us(50); //Random number here
		PORTDbits.RD4 = 1;
	}
	
}

void SPI_Register_To_Latch(char display){
	
	//Can only be executed when NOT transmitting
	if(display == DISPLAYR){
		TRISCbits.TRISC4 = 1;
	}else{
		TRISDbits.TRISD4 = 1;
	}
	__delay_us(50); //No idea
	
	if(display == DISPLAYR){
		TRISCbits.TRISC4 = 0;
	}else{
		TRISDbits.TRISD4 = 0;
	}
	
}

void SPI_Register_To_Word(char display){
	
	//Can only be executed when NOT transmitting
	if(display == DISPLAYR){
		TRISCbits.TRISC4 = 0;
	}else{
		TRISDbits.TRISD4 = 0;
	}
	__delay_us(50); //No idea
	
	if(display == DISPLAYR){
		TRISCbits.TRISC4 = 1;
	}else{
		TRISDbits.TRISD4 = 1;
	}
	
}

void SPI_Write_Char(char byte){
	char tmp = SSPBUF; //Make sure to clear any received bits or somthing
	//SSPBUF = byte;
    SSPBUF = 255;
    while(!PIR1bits.SSPIF) //Should be done with send list and interrupt
		continue;
    PIR1bits.SSPIF = 0;
}

void SPI_Write_String(char display, char *str){
    if(display == DISPLAYR){
        PORTCbits.RC4 = 1;
    }else{
        PORTDbits.RD4 = 1;
    }
    
	while(*str != '\0'){
		SPI_Write_Char(*str);
		str++;
	}
    
    if(display == DISPLAYR){
        PORTCbits.RC4 = 0;
    }else{
        PORTDbits.RD4 = 0;
    }
}