#include "Relais.h"


void Relais_Setup(){
	
	//char i;
	//for(i = 0; i < RELAISCOUNT; i++){
	//	RELAISTRIS |= 1 << i;
	//}
    
    //1 means led off, 0 means led on
    
	RELAISTRIS = 0;
    RELAISLAT  = 15;
    
    
}

void Select_Relais(char number){
	Turn_Off_All_Relais();
	Turn_On_Relais(number);
}

void Turn_On_Relais(char number){
	
	///CBIT(RELAISLAT, number);
	
    char mask = 1 << number;
    mask = ~mask;
	RELAISLAT = mask;
	
}

void Turn_Off_All_Relais(){
	RELAISLAT = 15;
}

void Turn_Off_Relais(char number){
	
	//SBIT(RELAISTRIS, number);
	
    RELAISLAT |= 1 << number;
    
	//Stel number 2
	//char mask = 1 << number; //00000100
	//mask = ~mask;			 //11111011
	
	//RELAISTRIS &= mask;
	
	//00001101 register
	//11111011 mask
	
	//00001001 output
	
}


char Relais_Is_Enabled(char number){
	
	return GBIT(RELAISLAT, number);
	
	//return !(RELAISTRIS &= 1 << number);
	
}


void Toggle_Relais(char number){
	
	if(!Relais_Is_Enabled(number)){
		Turn_On_Relais(number);
	}else{
		Turn_Off_Relais(number);
	}
	
}
